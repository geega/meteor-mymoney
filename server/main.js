import { Meteor } from 'meteor/meteor';
import '../lib/collections.js';
import { Transactions} from "../lib/collections";

Meteor.startup(() => {
  // code to run on server at startup
});

Meteor.methods({

    /**
     * Get main chart data
     * @return {Promise<void>}
     */
    async getChartData() {
        let resultAggregateCursor = await Transactions.aggregate([
            {
                "$match" : {
                    "owner" : Meteor.userId()
                }
            },{
            "$group" : {
                "_id" : {
                    "d" : {
                        "$dateToString" : {
                            "format" : "%Y-%m-%d",
                            "date" : "$createdAt"
                        }
                    },
                    "type" : "$type"
                },
                "sum" : {
                    "$sum" : "$sum"
                }
            }
        },{
            "$group" : {
                "_id" : "$_id.d",
                "items" : {
                    "$push" : {
                        "type" : "$_id.type",
                        "sum" : "$sum"
                    }
                }
            }
        }]);

        return resultAggregateCursor.toArray();
    }
});