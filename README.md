# Meteor simple application 

## Auth data 
login: **meteor**
password: **qwerty**

[Full project with data hear](https://www.dropbox.com/s/e2oxkkal51f02b9/meteor-money.zip?dl=0)

### Helper installations:

1. Install bootstrap 
```
> meteor add twbs:bootstrap
```

2. Highcharts

```bash
> meteor npm install --save highcharts

> meteor add highcharts:highcharts-meteor
```

3. Accounts UI 

```bash

> meteor add accounts-ui accounts-password
```


4. Install 

```bash
 >meteor npm install --save bcrypt
```


5. Meteor collection aggrigate 

```bash
meteor add meteorhacks:aggregate
```

