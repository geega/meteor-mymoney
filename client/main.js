import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';
import { Transactions } from '../lib/collections';
import { Accounts } from 'meteor/accounts-base';


Accounts.ui.config({
    passwordSignupFields: 'USERNAME_ONLY',
});


Template.body.helpers({
    transactions() {
        return Transactions.find({owner: Meteor.userId()});
    }

});

Template.transaction.helpers({
    typeIs: function(type){
        return this.type === type;
    },
    formatDate: function(date){
        let d = new Date(date),
            month = d.getMonth() + 1,

            day = d.getDate().toString(),
            year = d.getFullYear().toString(),
            hour = d.getHours().toString(),
            min = d.getMinutes().toString(),
            sec = d.getSeconds().toString();


        if (month.toString().length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        if (hour.length < 2) hour = '0'+ hour;
        if (min.length < 2) min = '0'+ min;
        if (sec.length < 2) sec = '0'+ sec;

        return [year, month, day].join('-') + ' ' + [hour, min, sec].join(':');
    }
});


Template.addInModal.events({
    'submit .add-form-in': function () {
        event.preventDefault();


        let target = event.target;
        let sum = target.sum.value;
        let note = target.note.value;
        if(sum > 0 ) {
            sum = Math.abs(sum);
            Transactions.insert({
                sum: parseInt(sum),
                type: 'in',
                note: note,
                owner: Meteor.userId(),
                createdAt: new Date()
            });
        }



        target.sum.value = '';
        target.note.value = '';

        $('#modelAddIn').modal('hide');

        return false;
    }
});

Template.addOutModal.events({
    'submit .add-form-out': function () {
        event.preventDefault();


        let target = event.target;
        let sum = target.sum.value;
        let note = target.note.value;



        if(sum > 0 ) {
            sum = parseInt(sum);
            sum = Math.abs(sum);
            sum *= -1;


            Transactions.insert({
                sum: parseInt(sum),
                type: 'out',
                note: note,
                owner: Meteor.userId(),
                createdAt: new Date()
            });
        }


        target.sum.value = '';
        target.note.value = '';

        $('#modelAddOut').modal('hide');

        return false;
    }
});

Template.body.helpers({
    createChart: function () {
        // Use Meteor.defer() to craete chart after DOM is ready:
        Meteor.defer(function () {
            // Create standard Highcharts chart with options:
            Meteor.call('getChartData',(error, result) => {

                let inArr= [];
                let outArr = [];
                let dateArr = [];

                for (var i = 0; i < result.length; i++) {
                    dateArr.push(result[i]['_id']);
                    let inSum = 0;
                    let outSum = 0;
                    if(result[i]['items'].length) {
                        result[i]['items'].forEach(function (obj) {
                            if(obj.type === 'in') {
                                inSum = obj.sum;
                            }

                            if(obj.type === 'out') {
                                outSum =  Math.abs(obj.sum) ;
                            }
                        })

                        inArr.push(inSum);
                        outArr.push(outSum);
                    }
                }

                Highcharts.chart('chart', {
                    series: [{
                        type: 'column',
                        name: 'Incoming',
                        data: inArr
                    }, {
                        type: 'column',
                        name: 'Outgoing',
                        data: outArr
                    }],
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'My money'
                    },
                    xAxis: {
                        categories: dateArr,
                        crosshair: true
                    },
                    yAxis: {
                        //allowDecimals: false,
                        title: {
                            text: '$'
                        },

                    }
                });

            });
        });
    }
});
